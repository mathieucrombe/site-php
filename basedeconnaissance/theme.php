<?php ob_start();
$title = "theme";
require 'bdd/bddconfig.php';
$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);
?>

<?php
$contenue = ob_get_clean();
require 'gabarit/template.php';
?>