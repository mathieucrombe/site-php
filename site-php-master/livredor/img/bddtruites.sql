-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 16 sep. 2021 à 15:25
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bddtruites`
--

-- --------------------------------------------------------

--
-- Structure de la table `bassin`
--

DROP TABLE IF EXISTS `bassin`;
CREATE TABLE IF NOT EXISTS `bassin` (
  `idBassin` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `refCapteur` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idBassin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `bassin`
--

INSERT INTO `bassin` (`idBassin`, `nom`, `description`, `photo`, `refCapteur`) VALUES
(1, 'Bassin du Hem', '20 hectares dans le centre d\'Armentières.', 'bassins1.jpg', 'E4FD'),
(2, 'Bassin du Héron', '15 hectares au centre de Villeneuve-d\'Ascq.', 'bassins2.jpg', '842A'),
(3, 'Bassin de l\'arc en ciel', '5 hectares au coeur des flandres. ', 'bassins3.jpg', '850E'),
(4, 'Bassin du sel', 'C\'est ici qu\'est crée la majorité du sel de France', 'sardoche.jpg', 'SARD');

-- --------------------------------------------------------

--
-- Structure de la table `temperature`
--

DROP TABLE IF EXISTS `temperature`;
CREATE TABLE IF NOT EXISTS `temperature` (
  `idTemp` int(11) NOT NULL AUTO_INCREMENT,
  `temp` float NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idBassin` int(11) NOT NULL,
  PRIMARY KEY (`idTemp`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `temperature`
--

INSERT INTO `temperature` (`idTemp`, `temp`, `date`, `idBassin`) VALUES
(1, 11.3, '2015-03-21 07:05:20', 1),
(2, 11.7, '2015-03-21 11:00:00', 1),
(3, 11.8, '2015-03-21 15:00:00', 1),
(4, 9.8, '2015-03-21 03:00:00', 2),
(5, 10.1, '2015-03-21 07:00:00', 2),
(6, 10.2, '2015-03-21 19:00:00', 1),
(7, 10.3, '2015-03-21 21:00:00', 1),
(8, 10.3, '2015-03-21 11:00:00', 2),
(9, 10.3, '2015-03-21 15:00:00', 2),
(10, 10, '2015-03-21 19:00:00', 2),
(11, 9.8, '2015-03-21 03:00:00', 3),
(12, 10.3, '2015-03-21 07:00:00', 3),
(13, 10.5, '2015-03-21 11:00:00', 3),
(14, 10.6, '2015-03-21 15:00:00', 3),
(15, 120, '2021-09-13 09:38:43', 4),
(16, 22, '2021-09-13 09:38:43', 4),
(17, 53.21, '2021-09-13 09:39:35', 4);

-- --------------------------------------------------------

--
-- Structure de la table `userweb`
--

DROP TABLE IF EXISTS `userweb`;
CREATE TABLE IF NOT EXISTS `userweb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
