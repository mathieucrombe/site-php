<?php
require 'bdd/bddconfig.php';
$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);
require 'gabarit/template.php';

$listmessage = $objBdd->query("SELECT * FROM message order by date desc");
?>
<section>
    <article>
        <table>
            <thead>
                <tr>
                    <th colspan="2"> Les Messages</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($listmessage as $mess) { 
                    ?>
                    <tr>
                        <td><?php echo "Le : " . $mess["date"]; ?></td>
                        <td><?php echo $mess["message"] . " ( " . $mess["pseudo"] . " )" ?></td>
                    </tr>
                <?php
                }
                $listmessage->closeCursor();
                ?>
            </tbody>
        </table>
    </article>
    <article>
        <form method="POST" action="insertMessage.php">
            <fieldset>
                <legend>Ajouter votre commentaire au livre d'or</legend>
                Pseudo :
                <input type="text" name="pseudo" value="" placeholder="votre pseudo" required>
                Message :
                <textarea name="commentaire" rows="10" cols="40" placeholder="Votre commentaire" required></textarea>
                <input type="submit" value="Enregistrer">
            </fieldset>
        </form>
    </article>
</section>