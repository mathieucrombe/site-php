<?php
require 'bdd/bddconfig.php';
$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);

$listetheme = $objBdd->query("SELECT * FROM theme")
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <header class="main-head">
            <img src="img/logo.jpg" class="logo-banner" alt="Livre de Connaissance">
            <blockquote>Base de connaissance</blockquote>
            <div class="auth-container">
            <?php
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié
                    echo $_SESSION['logged_in']['pseudo'];
                ?>
                    <a href="logout.php" class="signin-link">deconnexion</a>
                <?php
                } else {
                    //Personne n'est authentifié
                    // affichage d'un lien pour se connecter
                ?>
                    <a href="login.php" class="signin-link">Connexion</a>
                <?php
                } ?>
            </div>
        </header>

        <nav class="main-nav">
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <?php
                foreach ($listetheme as $theme) {
                ?>
                    <li><a href="theme.php?idtheme=<?php echo $theme['idTheme']; ?>&titre=<?php echo $theme['nom']; ?>"><?php echo $theme["nom"]; ?></a></li>
                <?php
                }
                
                 ?>
            </ul>
        </nav>
        <?php echo $contenue; ?>
    </div>
</body>

</html>