<?php ob_start();
$title = "Base de connaissance";
session_start(); // ou dans les pages de contenu 

require 'bdd/bddconfig.php';
$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);



?>
<section class="main-content">
    <?php
    if (isset($_SESSION['logged_in']['login']) == TRUE) { //si log
        $listearticle = $objBdd->query("SELECT * FROM article order by datepub desc limit 0,5 ");
        foreach ($listearticle as $theme) {
    ?>

            <article class="art-main">
                <h2><?php echo $theme['titre']; ?></h2>
                <p> <?php echo substr($theme['texte'], 0, 100) . '...'; ?> </p>
                <a href="article.php?idarticle=<?php echo $theme['idArticle']; ?>&titre=<?php echo $theme['titre']; ?>&iduser=<?php echo $theme['idUser']; ?>"> suite de l'article
                </a>
            </article>

        <?php
        }
        $listearticle->closeCursor();
    } else {                                                 //si pas log
        $listearticle = $objBdd->query("SELECT * FROM article where acces like 'public' order by datepub desc limit 0,5 ");
        foreach ($listearticle as $theme) {
        ?>
            <article class="art-main">
                <h2><?php echo $theme['titre']; ?></h2>
                <p> <?php echo substr($theme['texte'], 0, 100) . '...'; ?> </p>
                <a href="article.php?idarticle=<?php echo $theme['idArticle']; ?>&titre=<?php echo $theme['titre']; ?>&iduser=<?php echo $theme['idUser']; ?>"> suite de l'article
                </a>
            </article>
    <?php
        }
        $listearticle->closeCursor();
    }
    ?>

</section>
<?php
$contenue = ob_get_clean();
require 'gabarit/template.php';
?>