<?php ob_start();
session_start(); // ou dans les pages de contenu 

require 'bdd/bddconfig.php';
$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);

$_GET["titre"];
if (isset($_GET["titre"])) {
    $title = strval($_GET["titre"]);
}

$idarticle = intval($_GET["idarticle"]);
$temps = $objBdd->prepare("SELECT * FROM article WHERE idArticle = ? ");
$temps->execute(array($idarticle));

$idauteur = intval($_GET["iduser"]);
$aut = $objBdd->prepare("SELECT * FROM user WHERE idUser = ? ");
$aut->execute(array($idauteur));

?>

<section class="main-content">
    <article class="art-main">
        <h1><?php echo $title ?></h1>
        <?php 
        foreach ($temps as $article)
        {
        ?>
        <p><?php foreach ($aut as $auteur){ echo $auteur['pseudo']." "; } echo $article['datepub']; ?></p>
        <p><?php echo "<br />".$article['texte'] ?></p>
        <?php 
        }
        ?>

    </article>
</section>

<?php
$contenue = ob_get_clean();
require 'gabarit/template.php';
?>