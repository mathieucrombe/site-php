<?php
require 'bdd/bddconfig.php';

$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);
$idbassin =htmlspecialchars($_POST['idbassin']) ;
$sql="DELETE FROM `bassin` WHERE `bassin`.`idBassin` = $idbassin";   //DELETE INTO bassin (nom, description, refCapteur) VALUES (:nom, :descript, :refcapteur)

$temps=$objBdd->prepare($sql);
$temps->execute();

$objBdd = null;
$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$page = 'bassins.php';
header("Location: http://$serveur$chemin/$page");
?>