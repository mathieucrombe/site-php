<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Exemple de mise en page avec CSS grid layout, fake content">
    <meta name="author" content="Gwénaël LAURENT">
    <meta name="robots" content="none">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
    <div class="container">
        <header class="main-head">
            <img src="images/logo.png" class="logo-banner" alt="Logo de la pisciculture du web">
            <blockquote>Le meilleur caviar de truite des Hauts-de-France</blockquote>
            <div class="auth-container">

                <?php
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié
                    echo $_SESSION['logged_in']['prenom'] . ' ' . $_SESSION['logged_in']['nom'];
                ?>
                    <a href="logout.php" class="signin-link">deconnexion</a>
                <?php
                } else {
                    //Personne n'est authentifié
                    // affichage d'un lien pour se connecter
                ?>
                    <a href="login.php" class="signin-link">Connexion</a>
                <?php
                } ?>


            </div>
        </header>

        <nav class="main-nav">
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="bassins.php">Les bassins</a></li>
                <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>
                <?php
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié
                    //affichage des liens vers les pages privées
                ?>
                    <li><a href="ajouterbassin.php">Ajouter un bassin</a></li>
                    <li><a href="supprbassin.php">Supprimer un bassin</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>

        <?php echo $contenue; ?>

        <aside class="side">
            <ul class="socialnetworks-links">
                <li>
                    <a href="https://www.facebook.com"><img src="images/Facebook.svg" alt="Facebook"></a>
                </li>
                <li>
                    <a href="https://twitter.com/"><img src="images/Twitter.svg" alt="Twitter"></a>
                </li>
                <li>
                    <a href="https://www.instagram.com/"><img src="images/Instagram.svg" alt="Instagram"></a>
                </li>
                <li>
                    <a href="https://www.youtube.com/"><img src="images/Youtube.svg" alt="Youtube"></a>
                </li>
            </ul>
            <div class="advert">
                <h3>Caviar à texture ferme</h3>
                <img src="images/caviar-ferme.jpg" alt="Des bocaux en verre contenant des oeufs de truites">
                <p>
                    L’ikura de truite à texture ferme est principalement livré en Europe occidentale et aux
                    États-Unis pour y être emballé dans des bocaux en verre. C'est un produit haut-de-gamme...
                </p>
            </div>
        </aside>
        <footer class="main-footer">
            <p>&copy; Pisciculture du Web Armentières - Tous droits réservés -
                <a href="#">Contact</a>
            </p>
        </footer>
    </div>
</body>

</html>