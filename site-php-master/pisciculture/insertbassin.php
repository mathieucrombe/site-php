<?php
require 'bdd/bddconfig.php';

$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);
$nom =htmlspecialchars($_POST['nom']) ;
$descript =htmlspecialchars($_POST['descript']) ;
$refcapteur =htmlspecialchars($_POST['refcapteur']) ;
$sql="INSERT INTO bassin (nom, description, refCapteur) VALUES (:nom, :descript, :refcapteur)";   //<script>alert('coucou');</script>

$temps=$objBdd->prepare($sql);
$temps->bindParam(':nom', $nom, PDO::PARAM_STR);
$temps->bindParam(':descript', $descript, PDO::PARAM_STR); 
$temps->bindParam(':refcapteur', $refcapteur, PDO::PARAM_STR); 
$temps->execute();

$objBdd = null;
$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$page = 'bassins.php';
header("Location: http://$serveur$chemin/$page");
?>