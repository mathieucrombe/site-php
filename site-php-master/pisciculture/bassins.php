<?php ob_start();
session_start(); // ou dans les pages de contenu 
require 'bdd/bddconfig.php';
$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);

$title = "Les Bassins";
?>

<section class="main-content">
    <article class="art-main">
        <h1>Les bassins de la pisciculture du Web</h1>
        <h2>– dans un écrin de verdure</h2>
        <p>Nos truites danoises éclosent dans nos écloseries continentales, où elles passent les premiers
            mois de leur vie. L’environnement contrôlé de ces élevages nous permet d’assurer les meilleures
            conditions possibles pour leur culture. Quand elles ont environ deux ans, la plupart des truites
            sont transportées à nos élevages marins, dans les eaux côtières propres et limpides du Danemark.
        </p>

        <p>Elles y passent 7 à 8 mois, jusqu'à ce qu'elles remplissent parfaitement les conditions pour la
            récolte. Elles sont transportées vivantes et avec le plus grand soin dans de grands bateaux à
            viviers jusqu’à notre propre site de traitement situé à Aarøsund, dans le Jutland-du-Sud.</p>
    </article>
    <section class="content-sec">
        <?php
        $listeBassins = $objBdd->query("SELECT * FROM bassin");
        while ($bassin = $listeBassins->fetch()) {
        ?>
            <article class="art-sec">
                <h2><?php echo $bassin['nom']; ?></h2>
                <img src=images/<?php echo $bassin['photo']; ?> alt= <?php echo $bassin['nom']; ?>>
                <p> <?php echo $bassin['description']; ?> </p>
                <a href="temperatures.php?idbassin=<?php echo $bassin['idBassin']; ?>&nombassin=<?php echo $bassin['nom']; ?>">
                    Voir les températures
                </a>
            </article>
        <?php
        }
        $listeBassins->closeCursor();
        ?>
    </section>
</section>

<?php
$objBdd = null;
$contenue = ob_get_clean();
require 'gabarit/template.php';
?>