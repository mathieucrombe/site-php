#include <Arduino.h>
#include <Wire.h>                  //pour accéder au bus I2C
#include <SparkFunBME280.h>        //inclusion de la lib BMP280
#include <WiFi.h>                  //inclus le wifi
#include <HTTPClient.h>            //inclus le client http
#define IDB 1

BME280 capteurBMP280;              //pour communiquer avec le capteur
int sdaBMP280 = 13;                //broche SDA BMP280
int sclBMP280 = 15;                //broche SCL BMP280
int addrI2CBMP280 = 0x77;          //adresses I2C BMP280
float pression = 0.0;              //pour stocker la pression
float temperature = 0.0;           //pour stocker la température
String msgConsole = "";            //message pour la console
const char *ssid = "tpsnir";       // SSID du réseau WiFi
const char *password = "t2p0s2n0"; // passphrase du réseau WiF
int id = 1;                        //id du bassin
String test = "idb=1&degre=11.3";

WiFiClient clientTCP;
HTTPClient clientHTTP;
int httpCode; //code de retour du serveur web

void setup()
{
  Serial.begin(115200);
  Wire.begin(sdaBMP280, sclBMP280);           //Conf de la liaison I2C
  capteurBMP280.setI2CAddress(addrI2CBMP280); //adresse I2C du BMP280
  if (capteurBMP280.beginI2C(Wire) == false)
    Serial.println("BMP280 : communication impossible");
  Serial.println("");
  Serial.println("Adresse MAC ESP = " + WiFi.macAddress());
  // Connexion au réseau WiFi
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("Connexion au SSID = " + (String)ssid + " ...");
  // Attendre que la connexion WiFi soit établie
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Nouvelle tentative");
  }
  // Quand on est connecté au réseau WiFi, le serveur DHCP attribue automatiquement la configuration IP
  Serial.println("Connection établie !");
  Serial.println("Adresse MAC ESP = " + WiFi.macAddress());
  Serial.println("Adresse IP ESP = " + WiFi.localIP().toString());
  Serial.println("Masque ESP = " + WiFi.subnetMask().toString());
  Serial.println("Passerelle ESP = " + WiFi.gatewayIP().toString());
  Serial.println("DNS1 ESP = " + WiFi.dnsIP(0).toString());
  Serial.println();
}
void loop()
{
  //Pression atmosphérique
  pression = capteurBMP280.readFloatPressure() / 100; // en hPa
  msgConsole = "Pression : " + (String)pression + " hPa - ";
  //température
  temperature = capteurBMP280.readTempC();
  msgConsole += "Température : " + (String)temperature + " °C";
  Serial.println(msgConsole);
  delay(500);
  //seulement si on est connecté au réseau WiFi
  if ((WiFi.status() == WL_CONNECTED))
  {
    String urlPost = "http://172.16.128.117/pisciculture/ws/temperatures/insert.php";
    //parsing the url for all needed parameters
    clientHTTP.begin(clientTCP, urlPost);
    //ajout des en-têtes HTTP
    clientHTTP.addHeader("Content-Type", "application/x-www-form-urlencoded");
    //donnée à envoyée au format JSON
    String postData = "idb=" + String(IDB) + "&degre="+ String(temperature);
    // start connection and send HTTP header and body
    httpCode = clientHTTP.POST(postData.c_str());
    // httpCode will be negative on error
    if (httpCode > 0)
    {
      if (httpCode == HTTP_CODE_OK) //200
      {
        Serial.println("Requête POST envoyée");
        String payload = clientHTTP.getString();
        Serial.println("Donnée reçue par HTTP POST : " + payload);
      }
    }
    else
    {
      Serial.printf("[HTTP] POST... failed, error: %s\n",
                    clientHTTP.errorToString(httpCode).c_str());
    }
    clientHTTP.end();
    delay(300000);
  }
  else
  {
    delay(30000); //on n'est pas connecté au réseau WiFi
  }
}
