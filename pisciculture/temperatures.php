<?php ob_start();
session_start(); // ou dans les pages de contenu 
require 'bdd/bddconfig.php';
$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);
$title = "temperatures";
$_GET["nombassin"];
if (isset($_GET["nombassin"])) {
    $nomBassin = strval($_GET["nombassin"]);
}

$idbassin = intval($_GET["idbassin"]);
$temps = $objBdd->prepare("SELECT * FROM temperature WHERE idBassin = ? order by date desc");
$temps->execute(array($idbassin));

?>
<section class="main-content">
    <article class="art-main">
        <h1> <?php
                if ($idbassin == 4) {
                    echo "La quantité de sel du " . $nomBassin;
                } else {
                    echo "Les températures du " . $nomBassin;
                }
                ?>
        </h1>
    </article>
    <section class="content-sec">
        <article class="art-sec">
            <table class="art-sec">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th><?php
                            if ($idbassin == 4) {
                                echo "Quantité de sel (Kg)";
                            } else {
                                echo "Température(°C)";
                            }
                            ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($bassin = $temps->fetch()) {
                    ?>

                        <tr>
                            <td><?php echo $bassin["date"]; ?></td>
                            <td><?php echo $bassin["temp"]; ?></td>
                        </tr>

                    <?php
                    }
                    $temps->closeCursor();
                    ?>
                </tbody>
            </table>
        </article>
    </section>
</section>



<?php
$objBdd = null;
$contenue = ob_get_clean();
require 'gabarit/template.php';
?>