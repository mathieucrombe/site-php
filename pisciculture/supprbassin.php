<?php
ob_start();
session_start(); // ou dans les pages de contenu 
$title = "Supprimer Bassin";
require 'bdd/bddconfig.php';

$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);

//Accès seulement si authentifié 
if (isset($_SESSION['logged_in']['login']) !== TRUE) {
    // Redirige vers la page d'accueil (ou login.php) si pas authentifié
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
}
// contenu de la page privée

?>
<section class="main-content">
    <article class="art-main">
        <table class="art-sec">
            <thead>
                <tr>
                    <th class="suppr">
                        Supprimer un bassin
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $listeBassins = $objBdd->query("SELECT * FROM bassin");
                foreach ($listeBassins as $bassin) {
                ?>
                    <tr>
                        <td><?php echo $bassin["nom"]; ?></td>
                        <td>
                            <form method="POST" action="deletebassin.php">
                                <input type="hidden" name="idbassin" value=<?php echo $bassin["idBassin"]; ?>>
                                <input type="submit" value="Supprimer">
                            </form>
                        </td>
                    </tr>
                <?php
                }
                $listeBassins->closeCursor();
                ?>
            </tbody>
        </table>
    </article>
</section>


<?php
$contenue = ob_get_clean();
require 'gabarit/template.php';
?>