<?php


if (isset($_GET["idb"]) == true) {
    require '../../bdd/bddconfig.php';

    $objBdd = new PDO("mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);

    $idb = intval(htmlspecialchars($_GET["idb"]));

    $requete = "SELECT idTemp, temp, date, idBassin FROM temperature WHERE idBassin = :id ORDER BY date DESC ";
    $temps = $objBdd->prepare($requete);
    $temps->bindParam(':id', $idb, PDO::PARAM_INT);
    $temps->execute();

    $temperature = array();
    foreach ($temps as $temp) {
        $arrayTemp = array(
            "idb" => $temp['idBassin'],
            "date" => $temp['date'],
            "temperature" => $temp['temp']
        );
        
        array_push($temperature,  $arrayTemp);
    }

    echo json_encode($temperature);


    // $taille = count($temperature);
    // for ($x = 0; $x < $taille; $x++) {
    //     echo $temperature[$x];
    //     echo "<br />";
    // }
    $temps->closeCursor();
     $objBdd = NULL;
} 
