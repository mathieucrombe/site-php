    <?php
    require '../../bdd/bddconfig.php';

    $objBdd = new PDO("mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);

    if (isset($_GET["idb"]) == true) {
        $idb = intval(htmlspecialchars($_GET["idb"]));

        $requete = "SELECT idTemp, temp, date, idBassin FROM `temperature` WHERE idBassin = :id ORDER BY date DESC LIMIT 0, 1";
        $temps = $objBdd->prepare($requete);
        $temps->bindParam(':id', $idb, PDO::PARAM_INT);
        $temps->execute();

        foreach ($temps as $temp) {
            $arrayTemp = array(
                "idb" => $temp['idBassin'],
                "date" => $temp['date'],
                "temperature" => $temp['temp']
            );
            $valRetourJson = json_encode($arrayTemp);
            echo $valRetourJson;
        }
        $temps->closeCursor();
    } else {
        $objBdd = NULL;
    }
