<?php
require '../../bdd/bddconfig.php';

$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);
if (isset ($_POST["idb"])){
    $idb = intval(htmlspecialchars($_POST["idb"])) ;
}
if (isset ($_POST["degre"])){
    $degre = floatval(htmlspecialchars($_POST["degre"])) ;
}

$sql="INSERT INTO temperature (temp, idBassin) VALUES (:temp, :id)";   

$temps=$objBdd->prepare($sql);
$temps->bindParam(':id', $idb, PDO::PARAM_INT);
$temps->bindParam(':temp', $degre, PDO::PARAM_STR); 
$temps->execute();

$objBdd = null;
?>