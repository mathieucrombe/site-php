<?php
require 'bdd/bddconfig.php';

$objBdd = new PDO(
    "mysql:hosthost=$bddserver;dbname=$bddname;charset=utf8",
    $bddlogin,
    $bddpass
);
$pseudo =htmlspecialchars($_POST['pseudo']) ;
$commentaire =htmlspecialchars($_POST['commentaire']) ;
$sql="INSERT INTO message (message, pseudo) VALUES (:commentaire, :pseudo)";   //<script>alert('coucou');</script>

$temps=$objBdd->prepare($sql);
$temps->bindParam(':commentaire', $commentaire, PDO::PARAM_STR);
$temps->bindParam(':pseudo', $pseudo, PDO::PARAM_STR); 
$temps->execute();

$objBdd = null;
$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$page = 'index.php';
header("Location: http://$serveur$chemin/$page");
?>